class plexmediaserver inherits ::plexmediaserver::params {

  require ::plexmediaserver::repo

  package { $package: ensure => installed, }
  service { $service:
    ensure => running,
    enable => true,
  }
}
