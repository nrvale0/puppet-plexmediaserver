class plexmediaserver::plugin::youtube( 
  $cache_time = 'CACHE_1HOUR'
) inherits plexmediaserver::plugin::youtube::params {
  include ::plexmediaserver::plugin

  validate_string($cacheTime)

  file_line { 'adjust YouTube plugin cache time':
    path  => $initsrc,
    line  => "  HTTP.CacheTime = ${cache_time}",
    match => '^  HTTP\.CacheTime = .+$',
  } 

  Service <| title == $plexmediaserver::service |> {
    subscribe => File_line['adjust YouTube plugin cache time'],
  }
}
