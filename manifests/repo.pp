class plexmediaserver::repo {

  include apt

  apt::source { 'Plex Media Server APT repo':
    location => 'http://shell.ninthgate.se/packages/debian',
    release => $::lsbdistcodename,
    repos => 'main',
    include_src => false,
  }

  Package <||> { require +> Apt::Source['Plex Media Server APT repo'], }
}
