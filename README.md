nrvale0-plexmediaserver
--------------
This module installs Plex Media Server on Debian.

# Usage
    include ::plexmediaserver
    
    # to adjust the YouTube channel refresh to 10 minutes.
    class { '::plexmediaserver::plugin::youtube':
      cache_time => 'CACHE_1HOUR /6'    
    }

# Dependencies
puppetlabs/stdlib version 4.1.0

# License
Apache 2.0 - http://www.apache.org/licenses/LICENSE-2.0.txt

# Contact
Nathan Valentine - nathan@puppetlabs.com|nrvale0@gmail.com

# Support
https://github.com/nrvale0/puppet-plexmediaserver
