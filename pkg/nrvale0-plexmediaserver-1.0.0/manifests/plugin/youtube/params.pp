class plexmediaserver::plugin::youtube::params {
  case $::operatingsystem {
    'debian': {
       $initsrc = '/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Plug-ins/YouTube.bundle/Contents/Code/__init__.py'
     }
     default: {fail("OS ${::operatingsystem} not supported by this module!")}
  }
}
