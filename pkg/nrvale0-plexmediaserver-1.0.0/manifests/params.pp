class plexmediaserver::params {
  case $::osfamily {
    'debian': {
      $package = 'plexmediaserver'
      $service = $package
    }
    default: {fail("OS family ${::osfamily} not supported by this module!")}
  }
}
